port module Main exposing (..)

import Array exposing (..)
import Browser
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Json.Decode as Decode





-- MAIN


main =
  Browser.element { init = \ md -> (Maybe.withDefault { projects = [ newProj "default" ], currentProject = "default"} md, Cmd.none), update = updateWithStorage, view = view, subscriptions = \_ -> Sub.none }

port setStorage : Model -> Cmd msg


updateWithStorage : Msg -> Model -> ( Model, Cmd Msg )
updateWithStorage msg model =
    let ( newModel ) = update msg model
    in
        ( newModel, Cmd.batch [ setStorage newModel ] )


-- MODEL

type alias Step = { checked : Bool
                  , easy : Bool
                  , label : Int
                  }

type alias Model = { projects : List Project
                   , currentProject : String
                   }

type alias Project = { steps : Array Step
                     , start : Int
                     , end : Int
                     , name : String
                     , goal : Int
                     }

genStep : Int -> Step
genStep i = { checked = False
            , easy = False
            , label = i
            }


newProj : String -> Project
newProj nam = { steps = Array.empty
              , start = 1
              , end = 0
              , goal = 1
              , name = nam
              }


actualGoalNum : Project -> Int
actualGoalNum proj = Array.foldl (\ s accu -> if not s.checked && s.label <= proj.goal then s.label else accu) (proj.start - 1) proj.steps

-- UPDATE

type ProjMsg
  = NewStart Int
  | NewEnd Int
  | Check Int
  | Easy Int
  | ResetChecked
  | NewGoal Int

type Msg
  = CurProj ProjMsg
  | NewProj String
  | SwitchProj String
  | Rename String
  | DeleteProj

updateIfPresent : Int -> (t -> t) -> Array t -> Array t
updateIfPresent i ch arr = Maybe.withDefault arr (Maybe.map ((\ v -> Array.set i v arr) << ch) (Array.get i arr))

appendSteps : Int -> Int -> Array Step -> Array Step
appendSteps num cur arr = if num <= 0
                          then arr
                          else appendSteps (num - 1) (cur + 1) (push (genStep cur) arr)

updateCur : Model -> (Project -> Project) -> Model
updateCur model chg = { model | projects = List.map (\ p -> if p.name == model.currentProject then chg p else p) model.projects }

isChecked : Int -> Project -> Bool
isChecked i proj = Maybe.withDefault False (Maybe.map (\ s -> s.checked) (Array.get (i - proj.start) proj.steps))

handleProjMsg : ProjMsg -> Project -> Project
handleProjMsg msg proj =
    case msg of
        NewStart ns -> let diff = ns - proj.start in { proj | start = ns, end = proj.end + diff, steps = Array.map (\ x -> { x | label = x.label + diff}) proj.steps }
        NewEnd ne -> if ne >= proj.start - 1 then let diff = ne - proj.end in { proj | end = ne, steps = if diff < 0 then slice 0 diff proj.steps else appendSteps diff (proj.end + 1) proj.steps } else proj
        Check i -> { proj | steps = updateIfPresent (i - proj.start) (\ x -> { x | checked = not x.checked}) proj.steps, goal = if i > proj.goal then if isChecked i proj then proj.goal + 1 else proj.goal - 1 else proj.goal }
        Easy i -> { proj | steps = updateIfPresent (i - proj.start) (\ x -> { x | easy = not x.easy}) proj.steps }
        ResetChecked -> { proj | steps = Array.map (\ x -> { x | checked = False}) proj.steps }
        NewGoal i -> { proj | goal = i }

update : Msg -> Model -> Model
update msg model =
    case msg of
        CurProj pm -> updateCur model (handleProjMsg pm)
        SwitchProj proj -> { model | currentProject = proj }
        NewProj proj -> { model | currentProject = proj, projects = newProj proj :: model.projects }
        Rename newname -> let newmod = updateCur model (\ p -> { p | name = newname}) in { newmod | currentProject = newname }
        DeleteProj -> let existingProjects = List.filter (\ p -> p.name /= model.currentProject) model.projects in { model | currentProject = Maybe.withDefault "default" (Maybe.map (\ p -> p.name) (List.head existingProjects)) , projects = existingProjects }



-- VIEW

withCurrentProject : Model -> (Project -> Html ProjMsg) -> Html Msg
withCurrentProject model f = Html.map CurProj (Maybe.withDefault (div [] []) (Maybe.map f (List.head (List.filter (\ p -> p.name == model.currentProject) model.projects))))


numInput ev def vf = input [ type_ "number", class "short", onInput (ev << Maybe.withDefault def << String.toInt), value (String.fromInt vf) ] []

rangeSelect : Project -> Html ProjMsg
rangeSelect proj =  span []
                    [ text "Range:"
                    , numInput NewStart 1 proj.start
                    , text "-"
                    , numInput NewEnd 0 proj.end
                    , text (" = " ++ String.fromInt (proj.end - proj.start + 1) ++ " Steps")
                    ]

resetButton proj = button [ onClick ResetChecked ] [ text "Reset" ]

goalSelect proj =  span []
                    [ text "Goal:"
                    , numInput NewGoal 1 proj.goal
                    ]

projectSelect : Model -> Html Msg
projectSelect model = span [ id "projselect"]
                      [ text "Project:"
                      , input [ onInput Rename, value model.currentProject ] []
                      , select [ onInput SwitchProj ] (List.map (\ p -> option [ selected (p.name == model.currentProject) ] [ text p.name ]) model.projects)
                      , button [ onClick (NewProj "new-project")] [ text "New"]
                      , button [ onClick DeleteProj ] [ text "Delete"]
                      ]

navbar : Model -> Html Msg
navbar model =
    nav [ class "fixed", class "top" ] [
          (withCurrentProject model rangeSelect)
        , text " "
        , (withCurrentProject model goalSelect)
        ,  projectSelect model
        ]

footbar : Model -> Html Msg
footbar model =
    footer [ class "fixed", class "bottom" ] [
         (withCurrentProject model resetButton)
        ]


step : Int -> Step -> Html ProjMsg
step gid stp = li [] [ span ([ preventDefaultOn "click" (Decode.succeed (Check stp.label, True)), preventDefaultOn "contextmenu" (Decode.succeed (Easy stp.label, True)), classList [ ("marked", stp.easy), ("done", stp.checked), ("goal", stp.label == gid)] ]
                        ) [ text (String.fromInt stp.label)]]

stepList : Model -> Html Msg
stepList model = main_ [ ]
                 [ (withCurrentProject model (\ p -> let gid = actualGoalNum p in ul [ id "stepList" ] (Array.toList (Array.map (step gid) p.steps))))
                 ]

view : Model -> Html Msg
view model =
  div []
    [ navbar model
    , stepList model
    , footbar model
    ]
