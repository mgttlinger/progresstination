let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {};
  def = import ./default.nix { nixpkgs = pkgs; };
in
pkgs.mkShell {
  buildInputs = with pkgs; [ elmPackages.elm-language-server elmPackages.elm elmPackages.elm-test elmPackages.elm-format elm2nix];
}
